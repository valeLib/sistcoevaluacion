from django.contrib import admin
from django.conf.urls import url, include

urlpatterns = [
    #path('admin/', admin.site.urls),
    url(r'^admin/', include(admin.site.urls))
]
